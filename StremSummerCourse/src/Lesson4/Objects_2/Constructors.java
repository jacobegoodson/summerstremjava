package Lesson4.Objects_2;

/**
 * Created by jacobgood1 on 6/15/2017.
 */
public class Constructors {
    int a;
    int b;

    Constructors(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public static void main(String[] args){
        Constructors constructor = new Constructors(1,2);
        System.out.println("a is " + constructor.a + " b is " + constructor.b);
    }
}
