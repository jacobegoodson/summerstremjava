package Lesson4.Objects_2;

/**
 * Created by jacobgood1 on 6/15/2017.
 */
public class Methods {
    // square takes a float, squares it, and returns a float, the square of n
    // n is called an argument or a parameter to a method
    static float square(float n){
        return n*n;
    }

    // removes the last character from the String s, then it returns that String s
    static String removeLast(String s){
        return s.substring(0,s.length() - 1);
    }

    public static void main(String[] args){
        String hiThere = removeLast("hi there");
        System.out.println(removeLast(hiThere));

        float finalSquare = square(square(3));
    }
}
