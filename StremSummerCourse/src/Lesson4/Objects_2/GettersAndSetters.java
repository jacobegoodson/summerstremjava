package Lesson4.Objects_2;

/**
 * Created by jacobgood1 on 6/15/2017.
 */
// getters and setters are a way of redirecting access to your fields
// this can be useful for refactoring(changing your code, hopefully, for the better)
// it can also be useful to give extra functionality to field access
public class GettersAndSetters {
    private int x;
    private int y;
    private int z;

    // right click, generate, getter and setter
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    // here we alter the access and setting of z
    public void setZ(int z) {
        this.z = z + 1;
    }

    public int getZ() {
        return z + 1;
    }
}
