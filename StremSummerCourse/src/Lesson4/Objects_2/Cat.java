package Lesson4.Objects_2;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
// Cat extends from Animal this means that it inherits all the fields and methods of Animal,
// it also means that Cat is an Animal, it is of the same type
// Animal is the super/parent class of Cat, Cat is the sub/child class of Animal
public class Cat extends Animal {
    // speak is in the base class Animal, Cat inherited this method from Animal
    // when a method is changed by a child class, we call this overriding, thus
    // speak is overridden by Cat




    void getStatus(){
        if(health >7 && health < 11){
            System.out.println("Cat is feelin' good!");
        }
        else if(health < 7 && health > 3) {
            System.out.println("Cat is not feelin too good...");
        }
    }

    // when speak is called from Cat it will run its specific code
    void speak(String meow){

        System.out.println(meow + " MEEEEEOOOOW!!!");

    }
}
