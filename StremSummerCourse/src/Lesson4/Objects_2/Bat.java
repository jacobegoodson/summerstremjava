package Lesson4.Objects_2;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
// Bat extends from Animal this means that it inherits all the fields and methods of Animal,
// it also means that Bat is an Animal, it is of the same type
// Animal is the super/parent class of Bat, Bat is the sub/child class of Animal
public class Bat extends Animal {
    // this is the constructor of Bat, the constructor is a method that runs when an object
    // is created/instantiated
    Bat(){
        // name is in the base class Animal, Bat inherited this field from Animal
        // when a field is changed by a child class, we call this overriding, thus
        // name is overridden by the constructor of Bat
        name = "Bat";
    }
}
