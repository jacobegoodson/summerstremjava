package Lesson4.Objects_2;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
// Animal is the BASE class for all the animal objects
class Animal {
    // Animal contains a field called name which is a string
    String name = "animal";
    int health = 10;

    void hurtAnimal(int damageAmount){
        health -= damageAmount;
    }
    // Animal contains a method speak, this is the DEFAULT BEHAVIOUR of the object
    void speak(){
        System.out.println(name + " makes a sound!");
    }
}
