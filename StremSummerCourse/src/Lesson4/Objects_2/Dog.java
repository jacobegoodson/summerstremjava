package Lesson4.Objects_2;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
public class Dog extends Animal {
    // speak is in the base class Animal, Dog inherited this method from Animal
    // when a method is changed by a child class, we call this overriding, thus
    // speak is overridden by Dog

    // when speak is called from Dog it will run its specific code
    void speak(){
        System.out.println("WOOOOF!");
    }
}
