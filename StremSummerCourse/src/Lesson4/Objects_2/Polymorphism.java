package Lesson4.Objects_2;

/**
 * Created by jacobgood1 on 6/07/2017.
 */

//refer to cat and dog classes
public class Polymorphism {
    public static void main(String[] args){
        Dog dog = new Dog();
        Cat cat = new Cat();
        Bat bat = new Bat();

        // run the specific speak for each instance
        dog.speak();
        cat.speak("cat call");
        bat.speak();

        cat.hurtAnimal(1000);
        System.out.println(cat.health);

        // every speak is different even though they all look the same .speak()
        // this is called polymorphism, meaning several different forms
    }
}
