package Lesson4.Methods_1;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by jacobgood1 on 6/07/2017.
 */

public class DragonCave {
    // scanner for gathering input from the user for the game
    static Scanner input = new Scanner(System.in);
    // display an intro for the game, static methods can only invoke other static methods, that is why
    // this method is static, because it is called by the main method which is static
    static void displayIntro(){
        System.out.println("You are in a land full of dragons. In front of you,");
        System.out.println("you see two caves. In one cave, the dragon is friendly");
        System.out.println("and will share his treasure with you. The other dragon");
        System.out.println("is greedy and hungry, and will eat you on sight.");
        System.out.println();
    }
    // method asks the user to choose a cave designated by the numbers 1 or 2, makes sure the
    // user enters either 1 or 2 before continuing
    static String chooseCave(){
        String caveChoice;

        System.out.println("Which cave will you go into? (1 or 2)");
        caveChoice = input.nextLine();

        // the user must choose 1 or 2, keep querying the user until they make the correct choice
        // this is called sanitizing the input
        while(!caveChoice.equals("1") && !caveChoice.equals("2")){ // Strings are compared with the .equals method
            System.out.println("Your choice was incorrect! Please choose 1 or 2...");
            caveChoice = input.nextLine();
        }

        return caveChoice;
    }
    // don't worry about throws, you have enough to learn already =)
    static void determineOutcome(String chosenCave) throws InterruptedException {
        Random random = new Random();

        System.out.println("You approach the cave...");
        // makes the program pause for 1000 milliseconds(1 second)... this will make the game
        // more suspenseful!
        Thread.sleep(1000);
        System.out.println("It is dark and spooky...");
        Thread.sleep(1000);
        System.out.println("A large dragon jumps out in front of you! He opens his jaws and...");
        System.out.println();
        Thread.sleep(1000);

        // victory is random
        int friendlyCave = random.nextInt(2) + 1;
        // since the choseenCave is a String it cannot be compared to an int, they are not the same type
        // chosenCave must be coerced/casted to an integer first
        if(Integer.parseInt(chosenCave) == friendlyCave){
            System.out.println("Gives you his treasure!");
        }
        else{
            System.out.println("Gobbles you down in one bite!");
        }
    }

    public static void main(String[] args) throws InterruptedException {
        String playAgain = "yes";
        // the contains method just looks for the character y in the String playAgain, if it is found
        // the game runs again, otherwise it exits
        while(playAgain.contains("y")){
            displayIntro();
            // methods can be passed as arguements, chooseCave returns a String, determineOutcome takes
            // a String
            determineOutcome(chooseCave());
            System.out.println("Do you want to play again? (y or n)");
            // the user only has to enter y, anything else and the game exits
            playAgain = input.nextLine();
        }
    }
}
