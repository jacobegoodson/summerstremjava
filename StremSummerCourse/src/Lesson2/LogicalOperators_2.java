package Lesson2;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
public class LogicalOperators_2 {
    public static void main(String[] args){

        // >: greater than
        if(1 > 2) {
            System.out.println("1 is greater than 2");
        }

        // <: less than
        if(1 < 2) {
            System.out.println("1 is less than 2");
        }

        // ==: is equal to
        if(1 == 2){
            System.out.println("1 is equal to 2");
        }

        // !=: is not equal to
        if(1 != 2){
            System.out.println("1 does not equal 2");
        }

        // >=: greater than or equal to
        if(1 >= 2){
            System.out.println("1 is greater than or equal to 2");
        }

        // <=: less than or equal to
        if(2 <= 1){
            System.out.println("2 is less than or equal to 1");
        }

        // &&: and, every expression must be true
        if(1 == 1 && 2 == 2){
            System.out.println("1 is equal to 1 and 2 is equal to 2");
        }

        // ||: or, at least one expression must be true, or short circuits
        if(1 != 1 || 1 == 1){
            System.out.println("1 does not equal 1 or 1 is equal to 1!");
        }
    }
}
