package Lesson2;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
public class Branching_1 {
    public static void main(String[] args){
        //if something is true, run the code in the {<HERE>}
        if(true){
            System.out.println("A true statement if there ever was one!");
        }
        //if nothing was true, run the else... else always runs if nothing is true!
        else{
            System.out.println("The first statement must not have been true!");
        }

        // declaring a boolean as a variable
        boolean notTrue = false;

        if(notTrue){
            System.out.println("This will not print");
        }
        else if(notTrue){
            System.out.println("neither will this");
        }
        else if(false){
            System.out.println("nor will this");
        }
        else{
            System.out.println("Since nothing was true, this will print!");
        }

    }
}
