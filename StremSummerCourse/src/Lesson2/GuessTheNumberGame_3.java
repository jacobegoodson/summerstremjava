package Lesson2;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
public class GuessTheNumberGame_3 {
    public static void main(String[] args){
        int guess = 0;
        // if no value is assigned to the declared variable it becomes a default type, it cannot be used
        // until a value is assigned to it
        String name;
        //this variable will hold the random number
        int randomNumber;
        // we need a random generator object, thankfully java has one from the java.util.Random library
        Random random = new Random();
        // Scanner for user input
        Scanner input = new Scanner(System.in);

        // generate the random number for the user to guess, from a range of 1 to 20
        randomNumber = random.nextInt(2) + 1;

        System.out.println("Hello! What is your name?");
        // Get a string input from the user, presumably their name
        name = input.nextLine();

        System.out.println("Well, " + name + " I am thinking of a number between 1 and 2");
        System.out.println("Take a guess as to what the number is: ");
        guess = input.nextInt();

        if(guess == randomNumber){
            System.out.println("That was correct! You win!");
        }
        else{
            System.out.println("Incorrect, you lose!");
        }
    }
}
