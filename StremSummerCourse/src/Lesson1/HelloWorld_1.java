package Lesson1;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
// public: accessible by everyone, private means accessible by the containing class
// class: a blueprint from which objects can be created
public class HelloWorld_1 {
    // static: means that the method is created one time in memory, and it is accessible without even
    // instantiating the object

    // void: is a type, basically meaning nothing... this is the return type of the method

    // String[]: is a type of array, an array of Strings

    // args: is the name of the String array that is being passed from the command line to main
    public static void main(String[] args){
        //.: The dot notation allows us to access the inner workings of objects, we are accessing System, then out
        //the the println method

        // println: displays a String to the user on the command line
        System.out.println("Hello World!");
    }
}


