package Lesson1;

import java.util.Scanner;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
public class WhatIsYourNumber_2 {
    public static void main(String[] args){
        //Scanner allows us to read input from the user
        Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.println("Enter your favorite number: ");
        //Scanner has a lot of useful ways of reading input, we coerce the input to an integer here
        int n = reader.nextInt(); // Scans the next token of the input as an int.
        //We can coerce the integer back to a string using String.valueOf
        String num = String.valueOf(n);
        //The plus sign can be used for string concatenation, joining strings together
        System.out.println("Your number is: " + num);
    }

}
