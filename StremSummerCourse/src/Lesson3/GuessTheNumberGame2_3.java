package Lesson3;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
public class GuessTheNumberGame2_3 {
    public static void main(String[] args){
        int guess;
        int maxGuesses = 6;
        boolean win = false;
        // if no value is assigned to the declared variable it becomes a default type, it cannot be used
        // until a value is assigned to it
        String name;
        //this variable will hold the random number
        int secretRandomNumber;
        // we need a random generator object, thankfully java has one from the java.util.Random library
        Random random = new Random();
        // Scanner for user input
        Scanner input = new Scanner(System.in);

        // generate the random number for the user to guess, from a range of 1 to 20
        secretRandomNumber = random.nextInt(20) + 1;

        System.out.println("Hello! What is your name?");
        // Get a string input from the user, presumably their name
        name = input.nextLine();

        System.out.println("Well, " + name + " I am thinking of a number between 1 and 20");


        System.out.println("Take a guess as to what the number is: ");

        for(int i = 0; i <= maxGuesses; i++){
            guess = input.nextInt();
            if(guess == secretRandomNumber){
                //if the guess was correct, the win trigger should be flipped to true
                win = true;
                // user has won, break out of the for loop, otherwise the game will keep going
                break;
            }
            // let the user know to guess higher
            else if(guess < secretRandomNumber){
                System.out.println("Wrong! Maybe you should guess higher...");
            }
            // let the user know to guess lower
            else if(guess > secretRandomNumber){
                System.out.println("Wrong! Maybe you should guess lower...");
            }

        }
        // if the player guess the right number the win boolean will be true, otherwise the else
        // code is run and the player is informed that they lost
        if(win){
            System.out.println("That was correct! You win!");
        }
        else{
            System.out.println(
                    "You have run out of guesses, the secret number was: "
                    + secretRandomNumber +
                    "... you lose!");
        }
    }
}
