package Lesson3;

import java.util.ArrayList;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
public class ArrayLists_2 {
    public static void main(String[] args){
        // This is a list, it contains integers, its initial capacity is 10
        ArrayList<Integer> arrayList = new ArrayList<Integer>(10);

        // to fill an array list, use a for loop
        for(int i = 1; i <= 10; i++){
            arrayList.add(i);
        }
        System.out.println(arrayList);

        // ArrayLists start at 0, not 1
        System.out.println(arrayList.get(0));
        // 1 is sort of like 2 here:
        System.out.println(arrayList.get(1));

        //new loop, foreach, useful for looping over data structures such as an array list
        // foreach will loop over every item in the arrayList
        arrayList.forEach((i) -> System.out.println(i));
    }
}
