package Lesson3;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
public class Loops_1 {
    public static void main(String[] args){
        //create a counter so that the loop has a reference of where to stop
        int counter = 1;
        // while counter is less than or equal to 10 keep printing the counter
        while(counter <= 10){
            System.out.println(counter);
            // inc the counter by 1
            counter++;
        }
        //reset the counter to 1 to use in another loop
        counter = 1;

        // demonstrate the break statement, it breaks out of a loop no matter what the current
        // condition is(true or false)
        while(counter <= 10){
            System.out.println(counter);
            counter++;
            //if the counter is equal to 7 leave the loop
            if(counter == 7){
                break;
            }
        }

        // for loop, set i to 1, while i is less than 10, increment i
        for(int i = 1; i <= 10; i++){
           System.out.println(i);
        }

        // for loops are very useful, you may be tempted to use while loops for everything but for loops
        // are easier to debug and maintain
    }
}
