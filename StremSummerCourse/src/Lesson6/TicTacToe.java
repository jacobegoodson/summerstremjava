package Lesson6;

/**
 * Created by jacobgood1 on 6/15/2017.
 */
public class TicTacToe {
    static String[] board = {" "," "," "," "," "," "," "," "," "," "};
    // This function prints out the board that it was passed.
    // "board" is a list(String array) of 10 strings
    // representing the board (ignore index 0)
    static void drawBoard(String[] board){
        System.out.println("   |   |");
        System.out.println(" " + board[7] + " | " + board[8] + " | " + board[9]);
        System.out.println("   |   |");
        System.out.println("----------");
        System.out.println("   |   |");
        System.out.println(" " + board[4] + " | " + board[5] + " | " + board[6]);
        System.out.println("   |   |");
        System.out.println("----------");
        System.out.println("   |   |");
        System.out.println(" " + board[1] + " | " + board[2] + " | " + board[3]);
        System.out.println("   |   |");
    }

    public static void main(String[] args){
        drawBoard(board);

    }
}
