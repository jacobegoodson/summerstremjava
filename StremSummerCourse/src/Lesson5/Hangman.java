package Lesson5;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by jacobgood1 on 6/07/2017.
 */
public class Hangman {
    //don't worry about this, it is just "art" for the game
    static String[] HANGMANPICS = {
            new StringBuilder()
                    .append("+---+\n")
                    .append("|   |\n")
                    .append("    |\n")
                    .append("    |\n")
                    .append("    |\n")
                    .append("    |\n")
                    .append("=========\n")
                    .toString(),
            new StringBuilder()
                    .append("+---+\n")
                    .append("|   |\n")
                    .append("O   |\n")
                    .append("    |\n")
                    .append("    |\n")
                    .append("    |\n")
                    .append("=========\n")
                    .toString(),
            new StringBuilder()
                    .append("+---+\n")
                    .append("|   |\n")
                    .append("O   |\n")
                    .append("|   |\n")
                    .append("    |\n")
                    .append("    |\n")
                    .append("=========\n")
                    .toString(),
            new StringBuilder()
                    .append(" +---+\n")
                    .append(" |   |\n")
                    .append(" O   |\n")
                    .append("/|   |\n")
                    .append("     |\n")
                    .append("     |\n")
                    .append("=========\n")
                    .toString(),
            new StringBuilder()
                    .append("  +---+\n")
                    .append("  |   |\n")
                    .append("  O   |\n")
                    .append(" /|\\  |\n")
                    .append("      |\n")
                    .append("      |\n")
                    .append("=========\n")
                    .toString(),
            new StringBuilder()
                    .append("  +---+\n")
                    .append("  |   |\n")
                    .append("  O   |\n")
                    .append(" /|\\  |\n")
                    .append("  |   |\n")
                    .append("      |\n")
                    .append("=========\n")
                    .toString(),
            new StringBuilder()
                    .append("  +---+\n")
                    .append("  |   |\n")
                    .append("  O   |\n")
                    .append(" /|\\  |\n")
                    .append("  |   |\n")
                    .append(" /    |\n")
                    .append("=========\n")
                    .toString(),
            new StringBuilder()
                    .append("  +---+\n")
                    .append("  |   |\n")
                    .append("  O   |\n")
                    .append(" /|\\  |\n")
                    .append("  |   |\n")
                    .append(" / \\  |\n")
                    .append("=========\n")
                    .toString(),
    };

    // a string can be turned into an array by using the split method, .split, .split takes a pattern to split with
    // here we use " " meaning to split this string into elements by
    static String[] words = "ant baboon badger bat bear beaver camel cat clam cobra cougar coyote crow deer dog donkey duck eagle ferret fox frog goat goose hawk lion lizard llama mole monkey moose mouse mule newt otter owl panda parrot pigeon python rabbit ram rat raven rhino salmon seal shark sheep skunk sloth snake spider stork swan tiger toad trout turkey turtle weasel whale wolf wombat zebra".split(" ");

    // used by various methods for user interaction with hangman
    static Scanner input = new Scanner(System.in);
    static Random random = new Random();
    static String getRandomWord(String[] wordArray){

        int wordIndex = random.nextInt(wordArray.length - 1);
        return wordArray[wordIndex];
    }
    // if the secret word is "ant" and two letters are correct such as "at"
    // this will display "a _ t"
    static void displayLettersLeft(String correctLetters, String secretWord){
        //create the amount blanks == the size of the secretWord
        String blanks = "";
        for(int i = 0; i < secretWord.length(); i++){
            blanks = blanks.concat("_");
        }

        // if the correct letters are in the secret word, change blanks at that index to the letter
        // from the secret word at the same index
        for(int i = 0; i < secretWord.length(); i++){
            if (correctLetters.contains(String.valueOf(secretWord.charAt(i)))){
                blanks = blanks.substring(0,i) + secretWord.charAt(i) + blanks.substring(i+1,blanks.length());
            }
        }

        // add spaces to the print so that correctLetters that are displayed are not on
        // top of each other
        for(int i = 0; i < blanks.length(); i++){
            System.out.print(blanks.charAt(i) + " ");
        }
        // extra space so the information displayed to the player is clear
        System.out.println();
    }

    static void displayBoard(String[] hangmanpics, String missedLetters, String correctLetters, String secretWord){
        // display the hangmanpic based on how many letters the player has missed
        System.out.println(hangmanpics[missedLetters.length()]);
        // create some space so that all the displayed information is clear to the player
        System.out.println();

        System.out.print("Missed Letters: ");
        for(int i = 0; i < missedLetters.length(); i++){
            System.out.print(missedLetters.charAt(i) + " ");
        }
        System.out.println();

        displayLettersLeft(correctLetters, secretWord);
        
    }

    // returns the letter the player entered, makes sure that it is not a letter that the player
    // has already guessed, also makes sure that the guess is an actual letter
    static String getGuess(String alreadyGuessed){
        String guess;
        while(true){
            System.out.println("Guess a letter.");
            guess = input.nextLine();
            // make sure the letter is lowercase
            guess = guess.toLowerCase();

            if(guess.length() != 1){
                System.out.println("Please enter a single letter!");
            }
            else if(alreadyGuessed.contains(guess)){
                System.out.println("You have already guessed that letter. Choose again.");
            }
            else if(!"abcdefghijklmnopqrstuvwxyz".contains(guess)){
                System.out.println("Please enter a LETTER.");
            }
            else{
                return guess;
            }
        }
    }
    // this method returns true if the player wants to play again
    static boolean playAgain(){
        System.out.println("Do you want to play again (y or n)");
        return input.nextLine().toLowerCase().contains("y");
    }

    public static void main(String[] args){
        System.out.println("HANGMAN");
        String missedLetters = "";
        String correctLetters = "";
        String secretWord = getRandomWord(words);
        boolean gameIsDone = false;
        boolean foundAllLetters = true;
        String guess;

        while(true){
            displayBoard(HANGMANPICS, missedLetters, correctLetters, secretWord);
            guess = getGuess(missedLetters + correctLetters);

            if(secretWord.contains(guess)){
                correctLetters = correctLetters + guess;
                foundAllLetters = true;

                for(int i = 0; i < secretWord.length(); i++){
                    if(!correctLetters.contains(String.valueOf(secretWord.charAt(i)))){
                        foundAllLetters = false;
                        break;
                    }
                }

                if(foundAllLetters){
                    System.out.println("You have found the secret word! " + secretWord + " You win!");
                    gameIsDone = true;
                }
            }
            else{
                missedLetters = missedLetters + guess;
                if(missedLetters.length() == HANGMANPICS.length){
                    System.out.println("You have run out of guesses! You lose!");
                    gameIsDone = true;
                }
            }

            if(gameIsDone){
                if(playAgain()){
                    missedLetters = "";
                    correctLetters = "";
                    gameIsDone = false;
                    secretWord = getRandomWord(words);
                }
                else{
                    break;
                }
            }
        }
    }
}
