package Lesson2.desktop;

import Lesson2.desktop.game.FlapppppityFlap_4;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher_2 {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		// we can configure the games width height and title here with the
		// conveniently named config object

		config.width = FlapppppityFlap_4.WIDTH;
		config.height = FlapppppityFlap_4.HEIGHT;
		config.title = FlapppppityFlap_4.TITLE;

		// launch the application
		new LwjglApplication(new FlapppppityFlap_4(), config);
	}
}
