package Lesson2.desktop.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by jacobgood1 on 6/14/2017.
 */
// this will be the start menu, it extends from state
// press alt insert to automatically override/implement
// the methods from State_1
// or right click and press generate
public class MenuState_3 extends State_1 {
    protected MenuState_3(GameStateManager_2 gameStateManager2) {
        super(gameStateManager2);
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch spriteBatch) {

    }
}
