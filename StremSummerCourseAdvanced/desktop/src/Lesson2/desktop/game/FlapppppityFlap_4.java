package Lesson2.desktop.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class FlapppppityFlap_4 extends ApplicationAdapter {
	public static final int WIDTH = 400;
	public static final int HEIGHT = 800;

	public static final String TITLE = "FLAPDATBURD!";

	// create a GameStateManager_4 field for managing the game states
	private GameStateManager_2 gameStateManager;
	private SpriteBatch batch;

	//remove the texture, no longer needed

	@Override
	public void create () {
		batch = new SpriteBatch();
		// initialize the GameStateManager_4
		gameStateManager = new GameStateManager_2();
		// remove the image, no longer needed

		// push the MenuState onto the StateManager
		// thus starting the game in the MenuState
		gameStateManager.push(new MenuState_3(gameStateManager));
		//move clear to here, it only needs to happen once
		Gdx.gl.glClearColor(1, 0, 0, 1);
	}

	@Override
	public void render () {
		// this wipes the screen clean, making a fresh slate to
		// draw(render) on
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// update and render are now being called in our gameStateManager
		// we "redirect" the render method to use our gsm
		gameStateManager.update(Gdx.graphics.getDeltaTime());
		gameStateManager.render(batch);
	}

	// remove dispose, it will be handled by our gameStateManager
}

// finally launch the game, you should see nothing
// but a red screen if all is well =)
