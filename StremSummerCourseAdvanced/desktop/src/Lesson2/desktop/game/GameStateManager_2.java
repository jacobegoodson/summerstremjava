package Lesson2.desktop.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

/**
 * Created by jacobgood1 on 6/14/2017.
 */
// manages the states of the game, such as menu, play, etc.
public class GameStateManager_2 {
    //stacks are like a stack of dishes, you put a bunch of dishes
    // on top of another, after a while you take a dish
    // off the top of the stack... stacks work the same way
    private Stack<State_1> states;
    // constructor initializes the states
    GameStateManager_2(){
        states = new Stack<State_1>();
    }

    // places a state on top of the stack: states
    public void push(State_1 state){
        states.push(state);
    }

    // removes the state that is at the top of the stack
    public void pop(){
        states.pop();
    }

    // sets the current state by popping the top one
    // and then pushing the state that is given as an argument
    public void set(State_1 state){
        states.pop();
        states.push(state);
    }

    // updates the state that is on top of the stack
    // takes delta time which is the change in time between frames
    public void update(float dt){
        states.peek().update(dt);
    }

    // renders the state that is on top of the stack
    public void render(SpriteBatch spriteBatch){
        states.peek().render(spriteBatch);
    }
}
