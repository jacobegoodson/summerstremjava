package Lesson2.desktop.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by jacobgood1 on 6/12/2017.
 */
// games are made up of game states, such as the pause
// state, when a player pauses the game
// a menu state, could be when the player is at the start screen
// ready to select play or options... etc.
public abstract class State_1 {
    //protected means: Package Private + can be seen by subclasses or package member.
    // Orthographic projection is a means of representing three-dimensional objects in two dimensions
    public OrthographicCamera cam;
    // Vectors are mathematical objects that contain direction and magnitude, they are ordered
    public Vector3 mouse;
    // GameStateManager_4 will handle the states that our game is currently in
    public GameStateManager_2 gameStateManager2;

    public State_1(GameStateManager_2 gameStateManager2){
        this.gameStateManager2 = gameStateManager2;
        cam = new OrthographicCamera();
        mouse = new Vector3();
    }

    // determines what happens when a user presses a button
    public abstract void handleInput();
    // updates the game at 1/60 a second: 60fps, all the game logic goes here
    // delta time: dt is the time it takes for a frame to be rendered
    public abstract void update(float dt);
    // render handles the drawing of all the objects that can be rendered in the game
    // SpriteBatches
    public abstract void render(SpriteBatch spriteBatch);
}
