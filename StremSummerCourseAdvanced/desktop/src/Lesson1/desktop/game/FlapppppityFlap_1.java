package Lesson1.desktop.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class FlapppppityFlap_1 extends ApplicationAdapter {
	// we need to set some static fields for our game their names
	// designate what they do
	public static final int WIDTH = 400;
	public static final int HEIGHT = 800;

	public static final String TITLE = "FLAPDATBURD!";

	// a batch will bundle images/textures together so that they are rendered
	// all at once, the gpu is really good at rendering things in batches
	SpriteBatch batch;
	// a texture is basically an image
	Texture img;

	// these are called annotations
	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("badlogic.jpg");
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		batch.draw(img, 0, 0);
		batch.end();
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}
}
