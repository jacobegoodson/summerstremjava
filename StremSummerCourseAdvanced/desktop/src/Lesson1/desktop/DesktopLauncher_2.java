package Lesson1.desktop;

import Lesson1.desktop.game.FlapppppityFlap_1;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher_2 {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		// we can configure the games width height and title here with the
		// conveniently named config object

		config.width = FlapppppityFlap_1.WIDTH;
		config.height = FlapppppityFlap_1.HEIGHT;
		config.title = FlapppppityFlap_1.TITLE;

		// launch the application
		new LwjglApplication(new FlapppppityFlap_1(), config);
	}
}
