package Lesson5.game.States;

import Lesson5.game.FlapppppityFlap;
import Lesson5.game.GameStateManager_4;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by jacobgood1 on 6/14/2017.
 */
public class MenuState_2 extends State_1 {

    private Texture background;
    private Texture playButton;

    public MenuState_2(GameStateManager_4 gameStateManager4) {
        super(gameStateManager4);
        background = new Texture("bg.png");
        playButton = new Texture("playBtn.png");
    }

    @Override
    public void handleInput() {
        //handle inputs
        if(Gdx.input.justTouched()){
            System.out.println("hi there");
            gameStateManager.set(new PlayState_3(gameStateManager));
            // get rid if the textures from this state by calling the dispose
            // that was implemented below
            dispose();
            // run the game and notice that it changes states
        }
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch spriteBatch) {

        spriteBatch.begin();
        spriteBatch.draw(background,0,0, FlapppppityFlap.WIDTH, FlapppppityFlap.HEIGHT);
        spriteBatch.draw(
                playButton,
                FlapppppityFlap.WIDTH / 2 - playButton.getWidth() / 2,
                FlapppppityFlap.HEIGHT / 2
                );
        spriteBatch.end();
    }
    // implement the dispose
    // we call this when we transition states
    @Override
    public void dispose() {
        background.dispose();
        playButton.dispose();
    }
}
