package Lesson5;

import Lesson5.game.FlapppppityFlap;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
//TODO this is a copy of lesson4 it needs to be done for Lesson5
//TODO use PART 6
public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = FlapppppityFlap.WIDTH;
		config.height = FlapppppityFlap.HEIGHT;
		config.title = FlapppppityFlap.TITLE;

		new LwjglApplication(new FlapppppityFlap(), config);
	}
}
