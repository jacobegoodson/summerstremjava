package Lesson3.game;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by jacobgood1 on 6/12/2017.
 */
public abstract class State {
    public OrthographicCamera cam;
    public Vector3 mouse;
    public GameStateManager gameStateManager2;

    public State(GameStateManager gameStateManager2){
        this.gameStateManager2 = gameStateManager2;
        cam = new OrthographicCamera();
        mouse = new Vector3();
    }

    public abstract void handleInput();
    public abstract void update(float dt);
    public abstract void render(SpriteBatch spriteBatch);
}
