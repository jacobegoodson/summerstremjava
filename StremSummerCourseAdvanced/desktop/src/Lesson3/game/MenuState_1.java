package Lesson3.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by jacobgood1 on 6/14/2017.
 */
public class MenuState_1 extends State {
    // we need the graphics for the menu state

    private Texture background;
    private Texture playButton;

    public MenuState_1(GameStateManager gameStateManager2) {
        super(gameStateManager2);
        // we need to instantiate the button and the background
        // to do that we have to load the assets from our folder
        // we type in the file name with the extension
        background = new Texture("bg.png");
        playButton = new Texture("playBtn.png");
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        // we need open then close our SpriteBatch
        // think of a SpriteBatch as a box, we need to open our
        // box place all the necessary artistic materials in the box
        // then we close the box and ship it off to the gpu
        // the gpu takes all the goodies inside and renders those to the
        // screen

        //open the box
        spriteBatch.begin();
        // start drawing background from 0,0... the bottom left corner of the screen
        // we use our settings from Flap... to set the size of the screen that is being drawn
        spriteBatch.draw(background,0,0, FlapppppityFlap.WIDTH, FlapppppityFlap.HEIGHT);
        // drawn playButton from the middle of the screen
        // it draws from the left side of the button so we need
        // to subtract 1/2 of the button width to make it centered
        // finally set the y the center as well
        spriteBatch.draw(
                playButton,
                FlapppppityFlap.WIDTH / 2 - playButton.getWidth() / 2,
                FlapppppityFlap.HEIGHT / 2
                );
        // close and ship the box
        spriteBatch.end();
    }
}
