package Lesson4.game.States;

import Lesson4.game.FlapppppityFlap;
import Lesson4.game.GameStateManager_4;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by jacobgood1 on 6/15/2017.
 */
public class PlayState_3 extends State_1{
    //create a bird texture
    private Texture bird;

    // generate getters setters and constructor
    public PlayState_3(GameStateManager_4 gameStateManager) {
        super(gameStateManager);
        //set the path to the bird
        bird = new Texture("bird.png");
        // zoom the camera into the corner where the happy flappy slappy bird is
        cam.setToOrtho(false, FlapppppityFlap.WIDTH / 2, FlapppppityFlap.HEIGHT / 2);
    }

    @Override
    public void handleInput() {

    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch spriteBatch) {
        // this command makes sure that the gpu will only draw those things
        // that are in the camera
        spriteBatch.setProjectionMatrix(cam.combined);
        // draw the bird
        spriteBatch.begin();
        spriteBatch.draw(bird,50, 50);
        spriteBatch.end();
    }

    @Override
    public void dispose() {

    }
}
