package Lesson4.game.States;

import Lesson4.game.GameStateManager_4;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by jacobgood1 on 6/12/2017.
 */
public abstract class State_1 {
    protected OrthographicCamera cam;
    protected Vector3 mouse;
    protected GameStateManager_4 gameStateManager;

    protected State_1(GameStateManager_4 gameStateManager){
        this.gameStateManager = gameStateManager;
        cam = new OrthographicCamera();
        mouse = new Vector3();
    }

    public abstract void handleInput();
    public abstract void update(float dt);
    public abstract void render(SpriteBatch spriteBatch);
    //TODO START
    // allows us to clean up the memory
    public abstract void dispose();
}
