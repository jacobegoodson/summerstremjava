package Lesson4.game;

import Lesson4.game.States.State_1;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Stack;

/**
 * Created by jacobgood1 on 6/14/2017.
 */
public class GameStateManager_4 {
    private Stack<State_1> states;
    GameStateManager_4(){
        states = new Stack<State_1>();
    }

    public void push(State_1 state1){
        states.push(state1);
    }
    public void pop(){
        states.pop();
    }
    public void set(State_1 state1){
        states.pop();
        states.push(state1);
    }

    public void update(float dt){
        // add the input to the update loop
        states.peek().handleInput();
        states.peek().update(dt);
    }
    public void render(SpriteBatch spriteBatch){
        states.peek().render(spriteBatch);
    }
}
