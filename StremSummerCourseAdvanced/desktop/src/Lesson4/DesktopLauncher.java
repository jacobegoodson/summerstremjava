package Lesson4;

import Lesson4.game.FlapppppityFlap;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = FlapppppityFlap.WIDTH;
		config.height = FlapppppityFlap.HEIGHT;
		config.title = FlapppppityFlap.TITLE;

		new LwjglApplication(new FlapppppityFlap(), config);
	}
}
